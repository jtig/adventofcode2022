import operator

with open('day1-challenge1-input.txt') as file:
    file_content = file.read()

#This was a dictionary. Should have been a list of dictionaries. Changed it.
elfBackpacks = []
elfBackpacksList = file_content.split('\n\n')

counter = 1
for backpackContents in elfBackpacksList:
    elfBackpacks.append({"elf": counter, "backpackContains": backpackContents.split('\n'), "totalCalories": 0})
    counter += 1


for elf in elfBackpacks:
    elf["totalCalories"] = sum(list(map(int, elf.get("backpackContains"))))
    # print("elf " + str(elf["elf"]) + " has " + str(elf["totalCalories"]) + " in their backpack")

top3List = sorted(elfBackpacks, key=operator.itemgetter('totalCalories'), reverse=True)[:3]

top3calories = 0
for elf in top3List:
    top3calories += elf.get("totalCalories")
print(top3calories)