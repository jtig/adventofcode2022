#open file
with open('day1-challenge1-input.txt') as file:
    file_content = file.read()

elfBackpacks = {}
elfBackpacksList = file_content.split('\n\n')

counter = 1
for backpackContents in elfBackpacksList:
    elfBackpacks[counter] = backpackContents.split('\n')
    counter += 1

elfWithMostCalories = 0
for elf in elfBackpacks.values():
    totalCalories = sum(list(map(int, elf)))
    if totalCalories > elfWithMostCalories:
        elfWithMostCalories = totalCalories
print(elfWithMostCalories)